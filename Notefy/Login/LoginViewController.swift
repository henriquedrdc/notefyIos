//
//  LoginViewController.swift
//  Notefy
//
//  Created by Henrique Drumond Rabelo da Costa on 2/26/16.
//  Copyright © 2016 Henrique Drumond Rabelo da Costa. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON
import EZLoadingActivity

class LoginViewController: UIViewController, UITextFieldDelegate
{
    @IBOutlet weak var backgroundHeightConstraint: NSLayoutConstraint!

    @IBOutlet weak var loginScrollView: UIScrollView!
    override func viewDidLoad()
    {
        super.viewDidLoad()
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: "dismissKeyboard")
        view.addGestureRecognizer(tap)
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning()
    {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    @IBAction func clickEntrarButton(sender: AnyObject)
    {
        EZLoadingActivity.show("Loading...", disableUI: true)
        Alamofire.request(.POST, "http://10.206.105.231/notefy/public/user/login", parameters: ["password":"12345", "email":"henriquedrdc@gmail.com"], encoding:.JSON).responseJSON
            { response in switch response.result {
            case .Success(let json):
                print("Success with JSON: \(json)")
                let json = JSON(data: response.data!)
                let userLoged = UserLoged(fromJson: json["data"][0])
                let subjectSelected = Subject(fromJson: json["data"][0]["subject"][0])
                //                print(teste.name)
                userLoged.setUserDefaults()
                subjectSelected.setSubjectSelectedDefault()
                EZLoadingActivity.hide(success: true, animated: true)
                let revealVc = self.storyboard?.instantiateViewControllerWithIdentifier("reveal") as! SWRevealViewController
                self.presentViewController(revealVc, animated: true, completion: nil)
                
            case .Failure(let error):
                EZLoadingActivity.Settings.FailText = "Fail, deu ruim, erro: \(error)"
                EZLoadingActivity.hide(success: false, animated: true)
                print("Request failed with error: \(error)")
                }
        }
        
    }
    func dismissKeyboard()
    {
        self.loginScrollView.contentInset = UIEdgeInsetsMake(0, 0, 0, 0)
        self.loginScrollView.setContentOffset(CGPointMake(0, 0), animated: true)
        //Causes the view (or one of its embedded text fields) to resign the first responder status.
        view.endEditing(true)
    }
    
    func textFieldDidBeginEditing(textField: UITextField)
    {
        self.loginScrollView.setContentOffset(CGPointMake(0, 60), animated: true)
        self.loginScrollView.contentInset = UIEdgeInsetsMake(0, 0, 216, 0)
//        self.backgroundHeightConstraint.constant += 216
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
