//
//  NotebookViewController.swift
//  Notefy
//
//  Created by Henrique Drumond Rabelo da Costa on 2/15/16.
//  Copyright © 2016 Henrique Drumond Rabelo da Costa. All rights reserved.
//

import UIKit

class NotebookViewController: UIViewController, UITableViewDelegate, UITableViewDataSource
{
    var isFromCamera:Bool = false
    var newImage:UIImage = UIImage()

    
    @IBOutlet weak var tableView: UITableView!
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        self.configView()
        // Do any additional setup after loading the view.
    }
    override func viewWillAppear(animated: Bool)
    {
        print("entrou")
        self.tableView.reloadData()
        
    }
    override func didReceiveMemoryWarning()
    {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        return 3
    }
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell
    {
        let cell = tableView.dequeueReusableCellWithIdentifier("noteCell", forIndexPath: indexPath) as! NoteTableViewCell
        cell.noteImageView.image = UIImage(named: "Pictures")
        if(isFromCamera && indexPath.row == 2)
        {
            cell.noteImageView.image = newImage
            cell.addButtom.alpha = 1
        }
        return cell
    }
    @IBAction func clickMenuButton(sender: AnyObject)
    {
        if self.revealViewController() != nil
        {
            revealViewController().revealToggle("revealToggle")
        }
    }
    func configView()
    {
        self.view.addGestureRecognizer(self.revealViewController().panGestureRecognizer())
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
