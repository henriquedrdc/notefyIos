//
//  ActivityViewController.swift
//  Notefy
//
//  Created by Henrique Drumond Rabelo da Costa on 2/15/16.
//  Copyright © 2016 Henrique Drumond Rabelo da Costa. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON

class ActivityViewController: UIViewController, UITableViewDelegate, UITableViewDataSource, SWRevealViewControllerDelegate
{
    @IBOutlet weak var menuButton: UIButton!
    @IBOutlet weak var subjectNameLabel: UILabel!

    override func viewDidLoad()
    {
        self.revealViewController().delegate = self
        self.setInfo()        
        super.viewDidLoad()
        self.configView()
        
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning()
    {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    func revealController(revealController: SWRevealViewController!, didMoveToPosition position: FrontViewPosition)
    {
        self.setInfo()
    }
    @IBAction func clickMenuButton(sender: AnyObject)
    {
        if self.revealViewController() != nil
        {
            revealViewController().revealToggle("revealToggle")
        }
    }
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        return 3
    }
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell
    {
        let cell = tableView.dequeueReusableCellWithIdentifier("activityCell", forIndexPath: indexPath) as! ActivityTableViewCell
        cell.noteImageView.image = UIImage(named: "Pictures")
        return cell
    }
    func setInfo()
    {
        if let subjectName = Subject.currentSubjectSelected()?.name
        {
            self.subjectNameLabel.text = subjectName
        }
    }
    func configView()
    {
        self.view.addGestureRecognizer(self.revealViewController().panGestureRecognizer())
    }

}
