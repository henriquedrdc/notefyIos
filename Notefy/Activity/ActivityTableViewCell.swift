//
//  ActivityTableViewCell.swift
//  Notefy
//
//  Created by Henrique Drumond Rabelo da Costa on 2/15/16.
//  Copyright © 2016 Henrique Drumond Rabelo da Costa. All rights reserved.
//

import UIKit

class ActivityTableViewCell: UITableViewCell
{
    @IBOutlet weak var activityDoneLabel: UILabel!

    @IBOutlet weak var noteImageView: UIImageView!
    @IBOutlet weak var profilePictureImageView: UIImageView!
    override func awakeFromNib()
    {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(selected: Bool, animated: Bool)
    {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
