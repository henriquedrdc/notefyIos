//
//  CameraViewController.swift
//  Notefy
//
//  Created by Henrique Drumond Rabelo da Costa on 2/15/16.
//  Copyright © 2016 Henrique Drumond Rabelo da Costa. All rights reserved.
//

import UIKit
import AVFoundation

class CameraViewController: UIViewController, UIImagePickerControllerDelegate, UINavigationControllerDelegate, UITabBarControllerDelegate
{
    var picker = UIImagePickerController()
    var arrayBarItems: [UITabBarItem] = []
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        self.configView()
        
        self.tabBarController?.delegate = self
    }
    override func viewWillAppear(animated: Bool)
    {
        print("entrou")
        
        
    }
    override func didReceiveMemoryWarning()
    {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    override func viewDidAppear(animated: Bool)
    {
        self.setImagePicker()
    }
    func setArrayBarItems()
    {
        let tabBarItems = self.tabBarController?.tabBar.items
        if let arrayOfTabBarItems = tabBarItems as! AnyObject as? [UITabBarItem]
        {
            self.arrayBarItems = arrayOfTabBarItems
        }
    }
    
    func setImagePicker()
    {
        if UIImagePickerController.availableCaptureModesForCameraDevice(.Rear) != nil
        {
            picker = UIImagePickerController() //make a clean controller
            picker.allowsEditing = false
            picker.sourceType = UIImagePickerControllerSourceType.Camera
            picker.cameraCaptureMode = .Photo
            picker.showsCameraControls = true
            picker.delegate = self
            //customView stuff
            let customViewController = CameraOverlayViewController(
                nibName:"CameraOverlayViewController",
                bundle: nil
            )
            let customView:CameraOverlayView = customViewController.view as! CameraOverlayView
//            customView.delegate = self
            customView.frame = self.picker.view.frame
            picker.modalPresentationStyle = .FullScreen
            presentViewController(picker, animated: true, completion: nil)
//            presentViewController(picker,animated: true,
//                completion: {
//                    self.picker.cameraOverlayView = customView
//                }
//            )
            
        }
        else
        {
            //no camera found -- alert the user.
            let alertVC = UIAlertController(
                title: "No Camera",
                message: "Sorry, this device has no camera",
                preferredStyle: .Alert)
            let okAction = UIAlertAction(
                title: "OK",
                style:.Default,
                handler: nil)
            alertVC.addAction(okAction)
            presentViewController(
                alertVC,
                animated: true,
                completion: nil)
        }
        
    }
    func imagePickerControllerDidCancel(picker: UIImagePickerController)
    {
        dismissViewControllerAnimated(true,completion: nil)
    }
    func imagePickerController(picker: UIImagePickerController,didFinishPickingMediaWithInfo info: [String : AnyObject])
    {
        if let image = info[UIImagePickerControllerOriginalImage]
        {
            let barViewControllers = self.tabBarController?.viewControllers
            let notebook = barViewControllers![1] as! NotebookViewController //20
            notebook.newImage = image as! UIImage
            notebook.isFromCamera = true
            self.tabBarController!.selectedIndex = 1;
            dismissViewControllerAnimated(true, completion: nil)
//            dismissViewControllerAnimated(true, completion: nil)
        }
        //        let chosenImage = info[UIImagePickerControllerOriginalImage] as! UIImage //2
        //        UIImageWriteToSavedPhotosAlbum(chosenImage, self,nil, nil)
    }
    func configView()
    {
        self.setArrayBarItems()
        self.view.addGestureRecognizer(self.revealViewController().panGestureRecognizer())
    }

}
