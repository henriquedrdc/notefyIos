//
//	UserLoged.swift
//
//	Create by Henrique da Costa on 23/2/2016
//	Copyright © 2016. All rights reserved.
//	Model file Generated using JSONExport: https://github.com/Ahmed-Ali/JSONExport

import Foundation
import SwiftyJSON

class UserLoged : NSObject, NSCoding{

	var email : String!
	var fbId : AnyObject!
	var id : Int!
	var name : String!
	var phone : String!
	var photoPath : AnyObject!
	var status : Int!
	var subject : [Subject]!
    private static var cUserLoged: UserLoged?
    
    
    static func currentUserLoged() -> UserLoged?
    {
        if let outData = NSUserDefaults.standardUserDefaults().dataForKey("userLoged")
        {
            let userLoged = NSKeyedUnarchiver.unarchiveObjectWithData(outData) as! UserLoged
            return userLoged
        }
        return nil
    }


	/**
	 * Instantiate the instance using the passed json values to set the properties values
	 */
	init(fromJson json: JSON!){
		if json == nil{
			return
		}
		email = json["email"].stringValue
		fbId = json["fb_id"].stringValue
		id = json["id"].intValue
		name = json["name"].stringValue
		phone = json["phone"].stringValue
		photoPath = json["photoPath"].stringValue
		status = json["status"].intValue
		subject = [Subject]()
		let subjectArray = json["subject"].arrayValue
		for subjectJson in subjectArray{
			let value = Subject(fromJson: subjectJson)
			subject.append(value)
		}
	}
    
    
	/**
	 * Returns all the available property values in the form of NSDictionary object where the key is the approperiate json key and the value is the value of the corresponding property
//	 */
//    func getFromUserDefaults() -> UserLoged?
//    {
//        
//        if let outData = NSUserDefaults.standardUserDefaults().dataForKey("userLoged")
//        {
//            let userLoged = NSKeyedUnarchiver.unarchiveObjectWithData(outData) as! UserLoged
////            email = userLoged.email
////            fbId = userLoged.fbId
////            id = userLoged.id
////            name = userLoged.name
////            phone = userLoged.phone
////            photoPath = userLoged.photoPath
////            status = userLoged.status
////            subject = userLoged.subject
//            return userLoged
//        }
//        return nil
//    }
    func setUserDefaults()
    {
        let data = NSKeyedArchiver.archivedDataWithRootObject(self)
        NSUserDefaults.standardUserDefaults().setObject(data, forKey: "userLoged")
    }
	func toDictionary() -> NSDictionary
	{
		var dictionary = NSMutableDictionary()
		if email != nil{
			dictionary["email"] = email
		}
		if fbId != nil{
			dictionary["fb_id"] = fbId
		}
		if id != nil{
			dictionary["id"] = id
		}
		if name != nil{
			dictionary["name"] = name
		}
		if phone != nil{
			dictionary["phone"] = phone
		}
		if photoPath != nil{
			dictionary["photoPath"] = photoPath
		}
		if status != nil{
			dictionary["status"] = status
		}
		if subject != nil{
			var dictionaryElements = [NSDictionary]()
			for subjectElement in subject {
				dictionaryElements.append(subjectElement.toDictionary())
			}
			dictionary["subject"] = dictionaryElements
		}
		return dictionary
	}

    /**
    * NSCoding required initializer.
    * Fills the data from the passed decoder
    */
    @objc required init(coder aDecoder: NSCoder)
	{
         email = aDecoder.decodeObjectForKey("email") as? String
         fbId = aDecoder.decodeObjectForKey("fb_id")! as AnyObject
         id = aDecoder.decodeObjectForKey("id") as? Int
         name = aDecoder.decodeObjectForKey("name") as? String
         phone = aDecoder.decodeObjectForKey("phone") as? String
         photoPath = aDecoder.decodeObjectForKey("photoPath")! as AnyObject
         status = aDecoder.decodeObjectForKey("status") as? Int
         subject = aDecoder.decodeObjectForKey("subject") as? [Subject]

	}

    /**
    * NSCoding required method.
    * Encodes mode properties into the decoder
    */
    @objc func encodeWithCoder(aCoder: NSCoder)
	{
		if email != nil{
			aCoder.encodeObject(email, forKey: "email")
		}
		if fbId != nil{
			aCoder.encodeObject(fbId, forKey: "fb_id")
		}
		if id != nil{
			aCoder.encodeObject(id, forKey: "id")
		}
		if name != nil{
			aCoder.encodeObject(name, forKey: "name")
		}
		if phone != nil{
			aCoder.encodeObject(phone, forKey: "phone")
		}
		if photoPath != nil{
			aCoder.encodeObject(photoPath, forKey: "photoPath")
		}
		if status != nil{
			aCoder.encodeObject(status, forKey: "status")
		}
		if subject != nil{
			aCoder.encodeObject(subject, forKey: "subject")
		}

	}

}