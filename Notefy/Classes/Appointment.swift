//
//	Appointment.swift
//
//	Create by Henrique da Costa on 23/2/2016
//	Copyright © 2016. All rights reserved.
//	Model file Generated using JSONExport: https://github.com/Ahmed-Ali/JSONExport

import Foundation
import SwiftyJSON

class Appointment : NSObject, NSCoding{

	var descriptionField : String!
	var dueDateTime : String!
	var id : Int!
	var name : String!
	var status : Int!
	var subjectId : Int!


	/**
	 * Instantiate the instance using the passed json values to set the properties values
	 */
	init(fromJson json: JSON!){
		if json == nil{
			return
		}
		descriptionField = json["description"].stringValue
		dueDateTime = json["dueDateTime"].stringValue
		id = json["id"].intValue
		name = json["name"].stringValue
		status = json["status"].intValue
		subjectId = json["subject_id"].intValue
	}

	/**
	 * Returns all the available property values in the form of NSDictionary object where the key is the approperiate json key and the value is the value of the corresponding property
	 */
	func toDictionary() -> NSDictionary
	{
		var dictionary = NSMutableDictionary()
		if descriptionField != nil{
			dictionary["description"] = descriptionField
		}
		if dueDateTime != nil{
			dictionary["dueDateTime"] = dueDateTime
		}
		if id != nil{
			dictionary["id"] = id
		}
		if name != nil{
			dictionary["name"] = name
		}
		if status != nil{
			dictionary["status"] = status
		}
		if subjectId != nil{
			dictionary["subject_id"] = subjectId
		}
		return dictionary
	}

    /**
    * NSCoding required initializer.
    * Fills the data from the passed decoder
    */
    @objc required init(coder aDecoder: NSCoder)
	{
         descriptionField = aDecoder.decodeObjectForKey("description") as? String
         dueDateTime = aDecoder.decodeObjectForKey("dueDateTime") as? String
         id = aDecoder.decodeObjectForKey("id") as? Int
         name = aDecoder.decodeObjectForKey("name") as? String
         status = aDecoder.decodeObjectForKey("status") as? Int
         subjectId = aDecoder.decodeObjectForKey("subject_id") as? Int

	}

    /**
    * NSCoding required method.
    * Encodes mode properties into the decoder
    */
    @objc func encodeWithCoder(aCoder: NSCoder)
	{
		if descriptionField != nil{
			aCoder.encodeObject(descriptionField, forKey: "description")
		}
		if dueDateTime != nil{
			aCoder.encodeObject(dueDateTime, forKey: "dueDateTime")
		}
		if id != nil{
			aCoder.encodeObject(id, forKey: "id")
		}
		if name != nil{
			aCoder.encodeObject(name, forKey: "name")
		}
		if status != nil{
			aCoder.encodeObject(status, forKey: "status")
		}
		if subjectId != nil{
			aCoder.encodeObject(subjectId, forKey: "subject_id")
		}

	}

}