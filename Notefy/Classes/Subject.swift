//
//	Subject.swift
//
//	Create by Henrique da Costa on 23/2/2016
//	Copyright © 2016. All rights reserved.
//	Model file Generated using JSONExport: https://github.com/Ahmed-Ali/JSONExport

import Foundation
import SwiftyJSON

class Subject : NSObject, NSCoding{

	var appointment : [Appointment]!
	var id : Int!
	var kind : String!
	var name : String!
	var photo : [Photo]!
	var status : Int!
	var user : [User]!
    private static var cSubjectSelect: Subject?
    
    
    static func currentSubjectSelected() -> Subject?
    {
        if let outData = NSUserDefaults.standardUserDefaults().dataForKey("subjectSelected")
        {
            let subjectSelected = NSKeyedUnarchiver.unarchiveObjectWithData(outData) as! Subject
            return subjectSelected
        }
        return nil
    }

	/**
	 * Instantiate the instance using the passed json values to set the properties values
	 */
	init(fromJson json: JSON!){
		if json == nil{
			return
		}
		appointment = [Appointment]()
		let appointmentArray = json["appointment"].arrayValue
		for appointmentJson in appointmentArray{
			let value = Appointment(fromJson: appointmentJson)
			appointment.append(value)
		}
		id = json["id"].intValue
		kind = json["kind"].stringValue
		name = json["name"].stringValue
		photo = [Photo]()
		let photoArray = json["photo"].arrayValue
		for photoJson in photoArray{
			let value = Photo(fromJson: photoJson)
			photo.append(value)
		}
		status = json["status"].intValue
		user = [User]()
		let userArray = json["user"].arrayValue
		for userJson in userArray{
			let value = User(fromJson: userJson)
			user.append(value)
		}
	}

	/**
	 * Returns all the available property values in the form of NSDictionary object where the key is the approperiate json key and the value is the value of the corresponding property
	 */
    func setSubjectSelectedDefault()
    {
        let data = NSKeyedArchiver.archivedDataWithRootObject(self)
        NSUserDefaults.standardUserDefaults().removeObjectForKey("subjectSelected")
        NSUserDefaults.standardUserDefaults().setObject(data, forKey: "subjectSelected")
    }
	func toDictionary() -> NSDictionary
	{
		var dictionary = NSMutableDictionary()
		if appointment != nil{
			var dictionaryElements = [NSDictionary]()
			for appointmentElement in appointment {
				dictionaryElements.append(appointmentElement.toDictionary())
			}
			dictionary["appointment"] = dictionaryElements
		}
		if id != nil{
			dictionary["id"] = id
		}
		if kind != nil{
			dictionary["kind"] = kind
		}
		if name != nil{
			dictionary["name"] = name
		}
		if photo != nil{
			var dictionaryElements = [NSDictionary]()
			for photoElement in photo {
				dictionaryElements.append(photoElement.toDictionary())
			}
			dictionary["photo"] = dictionaryElements
		}
		if status != nil{
			dictionary["status"] = status
		}
		if user != nil{
			var dictionaryElements = [NSDictionary]()
			for userElement in user {
				dictionaryElements.append(userElement.toDictionary())
			}
			dictionary["user"] = dictionaryElements
		}
		return dictionary
	}

    /**
    * NSCoding required initializer.
    * Fills the data from the passed decoder
    */
    @objc required init(coder aDecoder: NSCoder)
	{
         appointment = aDecoder.decodeObjectForKey("appointment") as? [Appointment]
         id = aDecoder.decodeObjectForKey("id") as? Int
         kind = aDecoder.decodeObjectForKey("kind") as? String
         name = aDecoder.decodeObjectForKey("name") as? String
         photo = aDecoder.decodeObjectForKey("photo") as? [Photo]
         status = aDecoder.decodeObjectForKey("status") as? Int
         user = aDecoder.decodeObjectForKey("user") as? [User]

	}

    /**
    * NSCoding required method.
    * Encodes mode properties into the decoder
    */
    @objc func encodeWithCoder(aCoder: NSCoder)
	{
		if appointment != nil{
			aCoder.encodeObject(appointment, forKey: "appointment")
		}
		if id != nil{
			aCoder.encodeObject(id, forKey: "id")
		}
		if kind != nil{
			aCoder.encodeObject(kind, forKey: "kind")
		}
		if name != nil{
			aCoder.encodeObject(name, forKey: "name")
		}
		if photo != nil{
			aCoder.encodeObject(photo, forKey: "photo")
		}
		if status != nil{
			aCoder.encodeObject(status, forKey: "status")
		}
		if user != nil{
			aCoder.encodeObject(user, forKey: "user")
		}

	}

}