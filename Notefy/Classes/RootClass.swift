//
//	RootClass.swift
//
//	Create by Henrique da Costa on 23/2/2016
//	Copyright © 2016. All rights reserved.
//	Model file Generated using JSONExport: https://github.com/Ahmed-Ali/JSONExport

import Foundation
import SwiftyJSON

class RootClass : NSObject, NSCoding{

	var userLoged : [UserLoged]!


	/**
	 * Instantiate the instance using the passed json values to set the properties values
	 */
	init(fromJson json: JSON!){
		if json == nil{
			return
		}
		userLoged = [UserLoged]()
		let userLogedArray = json["userLoged"].arrayValue
		for userLogedJson in userLogedArray{
			let value = UserLoged(fromJson: userLogedJson)
			userLoged.append(value)
		}
	}

	/**
	 * Returns all the available property values in the form of NSDictionary object where the key is the approperiate json key and the value is the value of the corresponding property
	 */
	func toDictionary() -> NSDictionary
	{
		var dictionary = NSMutableDictionary()
		if userLoged != nil{
			var dictionaryElements = [NSDictionary]()
			for userLogedElement in userLoged {
				dictionaryElements.append(userLogedElement.toDictionary())
			}
			dictionary["userLoged"] = dictionaryElements
		}
		return dictionary
	}

    /**
    * NSCoding required initializer.
    * Fills the data from the passed decoder
    */
    @objc required init(coder aDecoder: NSCoder)
	{
         userLoged = aDecoder.decodeObjectForKey("userLoged") as? [UserLoged]

	}

    /**
    * NSCoding required method.
    * Encodes mode properties into the decoder
    */
    @objc func encodeWithCoder(aCoder: NSCoder)
	{
		if userLoged != nil{
			aCoder.encodeObject(userLoged, forKey: "userLoged")
		}

	}

}