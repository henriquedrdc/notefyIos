//
//	Photo.swift
//
//	Create by Henrique da Costa on 23/2/2016
//	Copyright © 2016. All rights reserved.
//	Model file Generated using JSONExport: https://github.com/Ahmed-Ali/JSONExport

import Foundation
import SwiftyJSON

class Photo : NSObject, NSCoding{

	var id : Int!
	var path : String!
	var priority : Int!
	var status : Int!
	var subjectId : Int!


	/**
	 * Instantiate the instance using the passed json values to set the properties values
	 */
	init(fromJson json: JSON!){
		if json == nil{
			return
		}
		id = json["id"].intValue
		path = json["path"].stringValue
		priority = json["priority"].intValue
		status = json["status"].intValue
		subjectId = json["subject_id"].intValue
	}

	/**
	 * Returns all the available property values in the form of NSDictionary object where the key is the approperiate json key and the value is the value of the corresponding property
	 */
	func toDictionary() -> NSDictionary
	{
		var dictionary = NSMutableDictionary()
		if id != nil{
			dictionary["id"] = id
		}
		if path != nil{
			dictionary["path"] = path
		}
		if priority != nil{
			dictionary["priority"] = priority
		}
		if status != nil{
			dictionary["status"] = status
		}
		if subjectId != nil{
			dictionary["subject_id"] = subjectId
		}
		return dictionary
	}

    /**
    * NSCoding required initializer.
    * Fills the data from the passed decoder
    */
    @objc required init(coder aDecoder: NSCoder)
	{
         id = aDecoder.decodeObjectForKey("id") as? Int
         path = aDecoder.decodeObjectForKey("path") as? String
         priority = aDecoder.decodeObjectForKey("priority") as? Int
         status = aDecoder.decodeObjectForKey("status") as? Int
         subjectId = aDecoder.decodeObjectForKey("subject_id") as? Int

	}

    /**
    * NSCoding required method.
    * Encodes mode properties into the decoder
    */
    @objc func encodeWithCoder(aCoder: NSCoder)
	{
		if id != nil{
			aCoder.encodeObject(id, forKey: "id")
		}
		if path != nil{
			aCoder.encodeObject(path, forKey: "path")
		}
		if priority != nil{
			aCoder.encodeObject(priority, forKey: "priority")
		}
		if status != nil{
			aCoder.encodeObject(status, forKey: "status")
		}
		if subjectId != nil{
			aCoder.encodeObject(subjectId, forKey: "subject_id")
		}

	}

}