//
//  MenuViewController.swift
//  Notefy
//
//  Created by Henrique Drumond Rabelo da Costa on 2/17/16.
//  Copyright © 2016 Henrique Drumond Rabelo da Costa. All rights reserved.
//

import UIKit

class MenuViewController: UIViewController, UICollectionViewDataSource, UICollectionViewDelegate
{

    @IBOutlet weak var subjectCollectionView: UICollectionView!
    var subjectArray:[Subject] = []
    
    @IBOutlet weak var userNameLabel: UILabel!
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        self.configView()
        self.revealViewController().rearViewRevealWidth = 280

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning()
    {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    func collectionView(collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int
    {
        return subjectArray.count
        
    }
//    func collectionView(collectionView: UICollectionView, didSelectItemAtIndexPath indexPath: NSIndexPath)
//    {
//        
//    }
    func collectionView(collectionView: UICollectionView, didSelectItemAtIndexPath indexPath: NSIndexPath)
    {
        if indexPath.row != self.subjectArray.count
        {
            if let selectedSubject = Subject.currentSubjectSelected()
            {
                if selectedSubject.id != self.subjectArray[indexPath.row].id
                {
                    if let userLogedSubjects = UserLoged.currentUserLoged()!.subject
                    {
                        for subject in userLogedSubjects
                        {
                            if subject.id == self.subjectArray[indexPath.row].id
                            {
                                print("SUbject id \(subject.id)")
                                let newSubjectSelected = self.subjectArray[indexPath.row]
                                newSubjectSelected.setSubjectSelectedDefault()
//                                self.refreshUI()
                                self.subjectCollectionView.reloadData()
//                                let activityVc = self.storyboard?.instantiateViewControllerWithIdentifier("activityVc") as! ActivityViewController
//                                activityVc.subjectNameLabel.text = newSubjectSelected.name
                                revealViewController().revealToggle("revealToggle")
                            }
                            
                        }
                    }
                }
            }
 
        }
        else
        {
            
        }
        
    }
    func collectionView(collectionView: UICollectionView, cellForItemAtIndexPath indexPath: NSIndexPath) -> UICollectionViewCell
    {
        if(indexPath.row == self.subjectArray.count)
        {
            let cell = collectionView.dequeueReusableCellWithReuseIdentifier("addSubjectCell", forIndexPath: indexPath) as! AddSubjectCollectionViewCell
            
            return cell
        }
        let cell = collectionView.dequeueReusableCellWithReuseIdentifier("subjectCell", forIndexPath: indexPath) as! SubjectCollectionViewCell
        cell.subjectNameLabel.text = self.subjectArray[indexPath.row].name
        if let currentSubject = Subject.currentSubjectSelected()
        {
            print("currentSubject id \(currentSubject.id)")
            print("indexPath id \(self.subjectArray[indexPath.row].id)")
            if(currentSubject.id == self.subjectArray[indexPath.row].id)
            {
                cell.backgroundColor = UIColor.greenColor()
                print("colori")

            }
            else
            {
                cell.backgroundColor = UIColor.clearColor()
            }
        }
        
        return cell

    }
    func refreshUI() {
        dispatch_async(dispatch_get_main_queue(),{
            self.subjectCollectionView.reloadData()
        });
    }
    func configView()
    {
        if let currentUser = UserLoged.currentUserLoged()
        {
            self.userNameLabel.text = currentUser.name
            self.subjectArray = currentUser.subject
            self.subjectCollectionView.reloadData()
        }
        
    }
//    func numberOfSectionsInCollectionView(collectionView: UICollectionView) -> Int
//    {
//        return 1
//    }
    


}
