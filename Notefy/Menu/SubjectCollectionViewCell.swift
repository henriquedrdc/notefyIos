//
//  SubjectCollectionViewCell.swift
//  Notefy
//
//  Created by Henrique Drumond Rabelo da Costa on 2/17/16.
//  Copyright © 2016 Henrique Drumond Rabelo da Costa. All rights reserved.
//

import UIKit

class SubjectCollectionViewCell: UICollectionViewCell {
    

    @IBOutlet weak var bkgView: UIView!
    @IBOutlet weak var subjectNameLabel: UILabel!
    
    @IBOutlet weak var borderView: UIView!
    override func awakeFromNib()
    {
        self.layer.borderWidth = 0
    }
}
