//
//  Util.swift
//  Outlet
//
//  Created by Henrique Drumond Rabelo da Costa on 1/25/16.
//  Copyright © 2016 Henrique Drumond Rabelo da Costa. All rights reserved.
//

import Foundation




struct Util
{
    static var SERVER = "http://localhost/notefy/public/"
    static var TUDAO_SUBJECT = "get/tudao/subject/"
    static var LOGIN = "user/login"
    func md5(string string: String) -> String {
        var digest = [UInt8](count: Int(CC_MD5_DIGEST_LENGTH), repeatedValue: 0)
        if let data = string.dataUsingEncoding(NSUTF8StringEncoding) {
            CC_MD5(data.bytes, CC_LONG(data.length), &digest)
        }
        
        var digestHex = ""
        for index in 0..<Int(CC_MD5_DIGEST_LENGTH) {
            digestHex += String(format: "%02x", digest[index])
        }
        
        return digestHex
    }
}
