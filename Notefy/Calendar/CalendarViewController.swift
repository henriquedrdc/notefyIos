//
//  CalendarViewController.swift
//  Notefy
//
//  Created by Henrique Drumond Rabelo da Costa on 2/17/16.
//  Copyright © 2016 Henrique Drumond Rabelo da Costa. All rights reserved.
//

import UIKit
import CVCalendar

class CalendarViewController: UIViewController, CVCalendarViewDelegate, CVCalendarMenuViewDelegate
{
    

    @IBOutlet var calendarView: CVCalendarView!
    @IBOutlet var menuView: CVCalendarMenuView!
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        self.configView()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning()
    {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func clickMenuButton(sender: AnyObject)
    {
        if self.revealViewController() != nil
        {
            revealViewController().revealToggle("revealToggle")
        }
    }
    override func viewDidLayoutSubviews() {
        calendarView.commitCalendarViewUpdate()
        menuView.commitMenuViewUpdate()
    }
    func configView()
    {
        self.view.addGestureRecognizer(self.revealViewController().panGestureRecognizer())
    }
    func presentationMode() -> CalendarMode
    {
        return CalendarMode.MonthView
    }
    func firstWeekday() -> Weekday
    {
        return Weekday.Sunday
        
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
